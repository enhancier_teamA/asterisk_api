<?php
/**************************************************************************************
 * @author          : Mangesh Kamble
 * @FileType        : Log file
 * @LastModified    : 06 Dec 2017 10:23 
 * @Description     : This file contains all the testing URL's for created AMI API's
 ***************************************************************************************/
/*----- Queues -----*/
// addQueue                     : http://localhost/asterisk_api/asterisk_ami/queues.php?Operation=addQueue&SecKey=2c41c5e95e052e9d03f423e9f578b9b3&AgentExten=2001&Queue=sales&AgentName=mangesh&Priority
// removeQueue                  : http://localhost/asterisk_api/asterisk_ami/queues.php?Operation=removeQueue&SecKey=2c41c5e95e052e9d03f423e9f578b9b3&AgentExten=2001&Queue=sales
// pauseQueue                   : http://localhost/asterisk_api/asterisk_ami/queues.php?Operation=pauseQueue&SecKey=2c41c5e95e052e9d03f423e9f578b9b3&AgentExten=2001&Queue=sales&Paused=true
// unpauseQueue                 : http://localhost/asterisk_api/asterisk_ami/queues.php?Operation=pauseQueue&SecKey=2c41c5e95e052e9d03f423e9f578b9b3&AgentExten=2001&Queue=sales&Paused=false
/*----- Calls -----*/
//Originate                     : http://localhost/asterisk_api_v1/calls.php?Operation=originate&AgentExten=2001&custNumber=8055506738&SecKey=2c41c5e95e052e9d03f423e9f578b9b3&callerid=1234567890
// Hangup                       : http://localhost/asterisk_api/asterisk_ami/calls.php?Operation=Hangup&SecKey=2c41c5e95e052e9d03f423e9f578b9b3&AgentExten=2001
// Park  (Incomplete)           : http://localhost/asterisk_api/asterisk_ami/calls.php?Operation=Park&SecKey=2c41c5e95e052e9d03f423e9f578b9b3&AgentExten=2001&Timeout=45
// Mute                         : http://localhost/asterisk_api/asterisk_ami/calls.php?Operation=Mute&SecKey=2c41c5e95e052e9d03f423e9f578b9b3&AgentExten=2001&MuteFlag=on&Type=all
// Unmute                       : http://localhost/asterisk_api/asterisk_ami/calls.php?Operation=Mute&SecKey=2c41c5e95e052e9d03f423e9f578b9b3&AgentExten=2001&MuteFlag=off&Type=all


//Listen                        : http://localhost/asterisk_api/asterisk_ami/calls.php?Operation=Listen&SecKey=2c41c5e95e052e9d03f423e9f578b9b3&AgentExten=3001&Target=2002
//Whisper                       : http://localhost/asterisk_api/asterisk_ami/calls.php?Operation=Whisper&SecKey=2c41c5e95e052e9d03f423e9f578b9b3&AgentExten=3001&Target=2002
//Barge                         : http://localhost/asterisk_api/asterisk_ami/calls.php?Operation=Barge&SecKey=2c41c5e95e052e9d03f423e9f578b9b3&AgentExten=3001&Target=2002