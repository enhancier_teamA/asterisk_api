<?php

//===========================================================================
// ‘Copyright © 2017, Enhancier Cx Solutions Pvt Ltd.  All rights reserved.
//  File Description          : Agent queue mapping module,telephony location
//  Version                   : V 1.0
//  Current Path              :  http://localhost/asterisk_api_v1/queueFunctions.php
// --------------------------------------------------------------------------
//  Creation Details
//  Date Created              : 05 Dec 2017 12:23
//  Author                    : Mangesh Kamble
// --------------------------------------------------------------------------
//  Change History #1
//  Date Modified             : 
//  Changed By                : 
//  Change Description        : 
//  Ticket Ref Number         : 
//  Reason for Change         : 
//  Updated Code              : 
// --------------------------------------------------------------------------
//error_reporting(0);  // Turn off for testing
require "authenticate.php";

class Queues extends Auth {

    public function processApi() {
        if (isset($_GET['Operation'])) {
            $func = strtolower(trim(str_replace("/", "", $_GET['Operation'])));
            if ((int) method_exists($this, $func) > 0) {
                $socket = $this->authenticate_manager();
                $this->$func($socket);
            } else {
                echo json_encode(array('Code' => 404, 'Message' => 'Function not found'));
            }
        } else {
            echo json_encode(array('Code' => 404, 'Message' => 'Function not found'));
        }
    }

    private function addQueue($socket) {
        $SecKey = isset($_GET['SecKey']) ? $_GET['SecKey'] : '';
        $exten = isset($_GET['AgentExten']) ? $_GET['AgentExten'] : '';
        $Queue = isset($_GET['Queue']) ? $_GET['Queue'] : '';
        $tech = 'sip';
        $user = isset($_GET['AgentName']) ? $_GET['AgentName'] : '';
        $priority = isset($_GET['Priority']) ? $_GET['Priority'] : 2;

        if ($SecKey == '2c41c5e95e052e9d03f423e9f578b9b3') {
            $originateRequest = "Action: QueueAdd\r\n";
            $originateRequest .= "Queue: $Queue\r\n";
            $originateRequest .= "Interface: $tech/$exten\r\n";
            $originateRequest .= "MemberName: $user\r\n";
            $originateRequest .= "Penalty: $priority\r\n";
            $originateRequest .= "Paused: false\r\n";
            $originateRequest .= "Async: yes\r\n\r\n";

            # ENABLE DURING TESTING 
            #echo "<br> Input stream to Socket :<br>" . $originateRequest . "<br>";
            $originate = stream_socket_sendto($socket, $originateRequest);
            if ($originate > 0) {
                $originateResponse = fread($socket, "5038");
                if (isset($_GET['callback'])) {
                    echo $_GET['callback'] . '(' . json_encode(array("Code" => 0, "Message" => "Success")) . ')';
                } else {
                    echo json_encode(array("Code" => 0, "Message" => "Success"));
                }
            }
        } else {
            if (isset($_GET['callback'])) {
                echo $_GET['callback'] . '(' . json_encode(array("Code" => 500, "Message" => "Authentication failed")) . ')';
            } else {
                echo json_encode(array("Code" => 500, "Message" => "Authentication Failed"));
            }
        }
    }

    private function removeQueue($socket) {
        $SecKey = isset($_GET['SecKey']) ? $_GET['SecKey'] : '';
        $exten = isset($_GET['AgentExten']) ? $_GET['AgentExten'] : '';
        $Queue = isset($_GET['Queue']) ? $_GET['Queue'] : '';
        $tech = 'sip';

        if ($SecKey == '2c41c5e95e052e9d03f423e9f578b9b3') {
            $originateRequest = "Action: QueueRemove\r\n";
            $originateRequest .= "Queue: $Queue\r\n";
            $originateRequest .= "Interface: $tech/$exten\r\n";
            $originateRequest .= "Async: yes\r\n\r\n";

            # ENABLE DURING TESTING 
            #echo "<br> Input stream to Socket :<br>" . $originateRequest . "<br>";
            $originate = stream_socket_sendto($socket, $originateRequest);
            if ($originate > 0) {
                $originateResponse = fread($socket, "5038");
                if (isset($_GET['callback'])) {
                    echo $_GET['callback'] . '(' . json_encode(array("Code" => 0, "Message" => "Success")) . ')';
                } else {
                    echo json_encode(array("Code" => 0, "Message" => "Success"));
                }
            }
        } else {
            if (isset($_GET['callback'])) {
                echo $_GET['callback'] . '(' . json_encode(array("Code" => 500, "Message" => "Authentication failed")) . ')';
            } else {
                echo json_encode(array("Code" => 500, "Message" => "Authentication Failed"));
            }
        }
    }

    private function pauseQueue($socket) {
        $SecKey = isset($_GET['SecKey']) ? $_GET['SecKey'] : '';
        $exten = isset($_GET['AgentExten']) ? $_GET['AgentExten'] : '';
        $Queue = isset($_GET['Queue']) ? $_GET['Queue'] : '';
        $pauseflag = isset($_GET['Paused']) ? $_GET['Paused'] : '';
        $tech = 'sip';
        $pausecode = isset($_GET['PauseCode']) ? $_GET['PauseCode'] : '';

        if ($SecKey == '2c41c5e95e052e9d03f423e9f578b9b3') {
            $originateRequest = "Action: QueuePause\r\n";
            $originateRequest .= "Queue: $Queue\r\n";
            $originateRequest .= "Interface: $tech/$exten\r\n";
            $originateRequest .= "Reason: $pausecode\r\n";
            $originateRequest .= "Paused: $pauseflag\r\n";
            $originateRequest .= "Async: yes\r\n\r\n";

            # ENABLE DURING TESTING 
            #echo "<br> Input stream to Socket :<br>" . $originateRequest . "<br>";
            $originate = stream_socket_sendto($socket, $originateRequest);
            if ($originate > 0) {
                $originateResponse = fread($socket, "5038");
                if (isset($_GET['callback'])) {
                    echo $_GET['callback'] . '(' . json_encode(array("Code" => 0, "Message" => "Success")) . ')';
                } else {
                    echo json_encode(array("Code" => 0, "Message" => "Success"));
                }
            }
        } else {
            if (isset($_GET['callback'])) {
                echo $_GET['callback'] . '(' . json_encode(array("Code" => 500, "Message" => "Authentication failed")) . ')';
            } else {
                echo json_encode(array("Code" => 500, "Message" => "Authentication Failed"));
            }
        }
    }

    private function write_log($logData) {
        $myFile = 'QueueFunctions_log_' . date('Y-m-d') . '.txt';
        $fh = fopen($myFile, 'a+') or die("Please provide permission to log file.");
        $content = $logData . PHP_EOL;
        fwrite($fh, $content);
    }

}

$api = new Queues;
$api->processApi();
?>
