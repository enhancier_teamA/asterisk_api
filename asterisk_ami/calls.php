<?php

//===========================================================================
// ‘Copyright © 2017, Enhancier Cx Solutions Pvt Ltd.  All rights reserved.
//  File Description          : Agent queue mapping module,telephony location
//  Version                   : V 1.0
//  Current Path              :  http://localhost/asterisk_api/asterisk_ami/calls.php
// --------------------------------------------------------------------------
//  Creation Details
//  Date Created              : 05 Dec 2017 22:52
//  Author                    : Mangesh Kamble
// --------------------------------------------------------------------------
//  Change History #1
//  Date Modified             : 06 Dec 2017 09:56
//  Changed By                : Mangesh Kamble
//  Change Description        : Added Mute / Unmute feature , pulled agent channel detection out making it common function
//  Ticket Ref Number         : 
//  Reason for Change         : 
//  Updated Code              : 
// --------------------------------------------------------------------------
//  Change History #2
//  Date Modified             : 06 Feb 2018 08:38
//  Changed By                : Mangesh Kamble
//  Change Description        : Added Originate function
//  Ticket Ref Number         : 
//  Reason for Change         : Added new functionality to dial out numbers
//  Updated Code              : originate()
// --------------------------------------------------------------------------
//error_reporting(0);  // Turn off for testing
require "authenticate.php";

class Calls extends Auth {

    public function processApi() {
        if (isset($_GET['Operation'])) {
            $func = strtolower(trim(str_replace("/", "", $_GET['Operation'])));
            if ((int) method_exists($this, $func) > 0) {
                $socket = $this->authenticate_manager();
                $this->$func($socket);
            } else {
                echo json_encode(array('Code' => 404, 'Message' => 'Function not found'));
            }
        } else {
            echo json_encode(array('Code' => 404, 'Message' => 'Function not found'));
        }
    }

    private function originate($socket) {
        $SecKey = isset($_GET['SecKey']) ? $_GET['SecKey'] : '';
        $exten = isset($_GET['AgentExten']) ? $_GET['AgentExten'] : '';
        $custNumber = isset($_GET['custNumber']) ? $_GET['custNumber'] : '';
        $Callerid = isset($_GET['callerid']) ? $_GET['callerid'] : '';
        $context = 'from-sip';
        $Priority = '1';

        if ($SecKey == '2c41c5e95e052e9d03f423e9f578b9b3') {
            $originateRequest = "Action: Originate\r\n";
            $originateRequest .= "Channel: SIP/$exten\r\n";
            $originateRequest .= "Context: $context\r\n";
            $originateRequest .= "Exten: $custNumber\r\n";
            $originateRequest .= "Callerid: $Callerid\r\n";
            $originateRequest .= "Timeout: 30000\r\n";
            $originateRequest .= "Priority: 1\r\n\r\n";
            echo "<br> Input stream to Socket :<br>" . $originateRequest . "<br>";
            $originate = stream_socket_sendto($socket, $originateRequest);
            if ($originate > 0) {
                $originateResponse = fread($socket, "5038");
                print_r($originateResponse);
                //exit;
                if (isset($_GET['callback'])) {
                    echo $_GET['callback'] . '(' . json_encode(array("Code" => 0, "Message" => "Success")) . ')';
                } else {
                    echo json_encode(array("Code" => 0, "Message" => "Success"));
                }
            }
        } else {
            if (isset($_GET['callback'])) {
                echo $_GET['callback'] . '(' . json_encode(array("Code" => 500, "Message" => "Authentication failed")) . ')';
            } else {
                echo json_encode(array("Code" => 500, "Message" => "Authentication Failed"));
            }
        }
    }

    private function Listen($socket) {
        $SecKey = isset($_GET['SecKey']) ? $_GET['SecKey'] : '';
        $exten = isset($_GET['AgentExten']) ? $_GET['AgentExten'] : '';
        $target = isset($_GET['Target']) ? $_GET['Target'] : '';
        $priority = 1;
        $context = 'from-spy';

        if ($SecKey == '2c41c5e95e052e9d03f423e9f578b9b3') {
            $channel = $this->getActiveChannel($socket, $exten);
            $originateRequest = "Action: Originate\r\n";
            $originateRequest .= "Channel: SIP/$exten\r\n";
            $originateRequest .= "Context: $context\r\n";
            $originateRequest .= "Exten: *222$target\r\n";
            $originateRequest .= "Priority: $priority\r\n";
            $originateRequest .= "Callerid: $target\r\n";
            $originateRequest .= "Async: yes\r\n\r\n";

            # ENABLE DURING TESTING 
            echo "<br> Input stream to Socket :<br>" . $originateRequest . "<br>";
            $originate = stream_socket_sendto($socket, $originateRequest);
            if ($originate > 0) {
                $originateResponse = fread($socket, "5038");
                if (isset($_GET['callback'])) {
                    echo $_GET['callback'] . '(' . json_encode(array("Code" => 0, "Message" => "Success")) . ')';
                } else {
                    echo json_encode(array("Code" => 0, "Message" => "Success"));
                }
            }
        } else {
            if (isset($_GET['callback'])) {
                echo $_GET['callback'] . '(' . json_encode(array("Code" => 500, "Message" => "Authentication failed")) . ')';
            } else {
                echo json_encode(array("Code" => 500, "Message" => "Authentication Failed"));
            }
        }
    }

    private function Whisper($socket) {
        $SecKey = isset($_GET['SecKey']) ? $_GET['SecKey'] : '';
        $exten = isset($_GET['AgentExten']) ? $_GET['AgentExten'] : '';
        $target = isset($_GET['Target']) ? $_GET['Target'] : '';
        $priority = 1;
        $context = 'from-spy';

        if ($SecKey == '2c41c5e95e052e9d03f423e9f578b9b3') {
            $channel = $this->getActiveChannel($socket, $exten);
            $originateRequest = "Action: Originate\r\n";
            $originateRequest .= "Channel: SIP/$exten\r\n";
            $originateRequest .= "Context: $context\r\n";
            $originateRequest .= "Exten: *223$target\r\n";
            $originateRequest .= "Priority: $priority\r\n";
            $originateRequest .= "Callerid: $target\r\n";
            $originateRequest .= "Async: yes\r\n\r\n";

            # ENABLE DURING TESTING 
            echo "<br> Input stream to Socket :<br>" . $originateRequest . "<br>";
            $originate = stream_socket_sendto($socket, $originateRequest);
            if ($originate > 0) {
                $originateResponse = fread($socket, "5038");
                if (isset($_GET['callback'])) {
                    echo $_GET['callback'] . '(' . json_encode(array("Code" => 0, "Message" => "Success")) . ')';
                } else {
                    echo json_encode(array("Code" => 0, "Message" => "Success"));
                }
            }
        } else {
            if (isset($_GET['callback'])) {
                echo $_GET['callback'] . '(' . json_encode(array("Code" => 500, "Message" => "Authentication failed")) . ')';
            } else {
                echo json_encode(array("Code" => 500, "Message" => "Authentication Failed"));
            }
        }
    }

    private function Barge($socket) {
        $SecKey = isset($_GET['SecKey']) ? $_GET['SecKey'] : '';
        $exten = isset($_GET['AgentExten']) ? $_GET['AgentExten'] : '';
        $target = isset($_GET['Target']) ? $_GET['Target'] : '';
        $priority = 1;
        $context = 'from-spy';

        if ($SecKey == '2c41c5e95e052e9d03f423e9f578b9b3') {
            $channel = $this->getActiveChannel($socket, $exten);
            $originateRequest = "Action: Originate\r\n";
            $originateRequest .= "Channel: SIP/$exten\r\n";
            $originateRequest .= "Context: $context\r\n";
            $originateRequest .= "Exten: *224$target\r\n";
            $originateRequest .= "Priority: $priority\r\n";
            $originateRequest .= "Callerid: $target\r\n";
            $originateRequest .= "Async: yes\r\n\r\n";

            # ENABLE DURING TESTING 
            echo "<br> Input stream to Socket :<br>" . $originateRequest . "<br>";
            $originate = stream_socket_sendto($socket, $originateRequest);
            if ($originate > 0) {
                $originateResponse = fread($socket, "5038");
                if (isset($_GET['callback'])) {
                    echo $_GET['callback'] . '(' . json_encode(array("Code" => 0, "Message" => "Success")) . ')';
                } else {
                    echo json_encode(array("Code" => 0, "Message" => "Success"));
                }
            }
        } else {
            if (isset($_GET['callback'])) {
                echo $_GET['callback'] . '(' . json_encode(array("Code" => 500, "Message" => "Authentication failed")) . ')';
            } else {
                echo json_encode(array("Code" => 500, "Message" => "Authentication Failed"));
            }
        }
    }

    private function Hangup($socket) {
        $SecKey = isset($_GET['SecKey']) ? $_GET['SecKey'] : '';
        $exten = isset($_GET['AgentExten']) ? $_GET['AgentExten'] : '';
        if ($SecKey == '2c41c5e95e052e9d03f423e9f578b9b3') {
            $channel = $this->getActiveChannel($socket, $exten);
            $originateRequest = "Action: Hangup\r\n";
            $originateRequest .= "Channel: $channel\r\n";
            $originateRequest .= "Cause: \r\n";
            $originateRequest .= "Async: yes\r\n\r\n";

            # ENABLE DURING TESTING 
            echo "<br> Input stream to Socket :<br>" . $originateRequest . "<br>";
            $originate = stream_socket_sendto($socket, $originateRequest);
            if ($originate > 0) {
                $originateResponse = fread($socket, "5038");
                //print_r($originateResponse);
                //exit;
                if (isset($_GET['callback'])) {
                    echo $_GET['callback'] . '(' . json_encode(array("Code" => 0, "Message" => "Success")) . ')';
                } else {
                    echo json_encode(array("Code" => 0, "Message" => "Success"));
                }
            }
        } else {
            if (isset($_GET['callback'])) {
                echo $_GET['callback'] . '(' . json_encode(array("Code" => 500, "Message" => "Authentication failed")) . ')';
            } else {
                echo json_encode(array("Code" => 500, "Message" => "Authentication Failed"));
            }
        }
    }

    private function Park($socket) {
        $temp_var = '';
        $timeout = 0;
        $temp = array();
        # INPUTS -------------
        $SecKey = isset($_GET['SecKey']) ? $_GET['SecKey'] : '';
        $exten = isset($_GET['AgentExten']) ? $_GET['AgentExten'] : '';
        $parkflag = isset($_GET['ParkFlag']) ? $_GET['ParkFlag'] : '0';
        if ($SecKey == '2c41c5e95e052e9d03f423e9f578b9b3') {
            $channel = $this->getActiveChannel($socket, $exten);
            $originateRequest = "Action: Park\r\n";
            $originateRequest .= "Channel: $channel\r\n";
            $originateRequest .= "TimeoutChannel: $channel\r\n";
            $originateRequest .= "Timeout: $parkflag\r\n";
            $originateRequest .= "Parkinglot: default\r\n";
            $originateRequest .= "Async: yes\r\n\r\n";

            # ENABLE DURING TESTING 
            echo "<br> Input stream to Socket :<br>" . $originateRequest . "<br>";
            $originate = stream_socket_sendto($socket, $originateRequest);
            if ($originate > 0) {
                $originateResponse = fread($socket, "5038");
                if (isset($_GET['callback'])) {
                    echo $_GET['callback'] . '(' . json_encode(array("Code" => 0, "Message" => "Success")) . ')';
                } else {
                    echo json_encode(array("Code" => 0, "Message" => "Success"));
                }
            }
        } else {
            if (isset($_GET['callback'])) {
                echo $_GET['callback'] . '(' . json_encode(array("Code" => 500, "Message" => "Authentication failed")) . ')';
            } else {
                echo json_encode(array("Code" => 500, "Message" => "Authentication Failed"));
            }
        }
    }

    private function Mute($socket) {
        $temp_var = '';
        $temp = array();
        # INPUTS -------------
        $SecKey = isset($_GET['SecKey']) ? $_GET['SecKey'] : '';
        $exten = isset($_GET['AgentExten']) ? $_GET['AgentExten'] : '';
        $state = isset($_GET['MuteFlag']) ? $_GET['MuteFlag'] : 'off';
        $direction = isset($_GET['Type']) ? $_GET['Type'] : 'all';

        if ($SecKey == '2c41c5e95e052e9d03f423e9f578b9b3') {
            $channel = $this->getActiveChannel($socket, $exten);
            $originateRequest = "Action: MuteAudio\r\n";
            $originateRequest .= "Channel: $channel\r\n";
            $originateRequest .= "Direction: $direction\r\n";
            $originateRequest .= "State: $state\r\n";
            $originateRequest .= "Async: yes\r\n\r\n";

            # ENABLE DURING TESTING 
            echo "<br> Input stream to Socket :<br>" . $originateRequest . "<br>";
            $originate = stream_socket_sendto($socket, $originateRequest);
            if ($originate > 0) {
                $originateResponse = fread($socket, "5038");
                //print_r($originateResponse);
                //exit;
                if (isset($_GET['callback'])) {
                    echo $_GET['callback'] . '(' . json_encode(array("Code" => 0, "Message" => "Success")) . ')';
                } else {
                    echo json_encode(array("Code" => 0, "Message" => "Success"));
                }
            }
        } else {
            if (isset($_GET['callback'])) {
                echo $_GET['callback'] . '(' . json_encode(array("Code" => 500, "Message" => "Authentication failed")) . ')';
            } else {
                echo json_encode(array("Code" => 500, "Message" => "Authentication Failed"));
            }
        }
    }

    private function Monitor($socket) {
        $temp_var = '';
        $temp = array();
        # INPUTS -------------
        $SecKey = isset($_GET['SecKey']) ? $_GET['SecKey'] : '';
        $exten = isset($_GET['AgentExten']) ? $_GET['AgentExten'] : '';
        $filename = strtotime(date('Y-m-d H:i:s'));
        $format = 'wav';
        $mix = true;

        if ($SecKey == '2c41c5e95e052e9d03f423e9f578b9b3') {
            $channel = $this->getActiveChannel($socket, $exten);
            $originateRequest = "Action: Monitor\r\n";
            $originateRequest .= "Channel: $channel\r\n";
            $originateRequest .= "File: $filename\r\n";
            $originateRequest .= "Format: $format\r\n";
            $originateRequest .= "Mix: $mix\r\n\r\n";

            # ENABLE DURING TESTING 
            echo "<br> Input stream to Socket :<br>" . $originateRequest . "<br>";
            $originate = stream_socket_sendto($socket, $originateRequest);
            if ($originate > 0) {
                $originateResponse = fread($socket, "5038");
                //print_r($originateResponse);
                //exit;
                if (isset($_GET['callback'])) {
                    echo $_GET['callback'] . '(' . json_encode(array("Code" => 0, "Message" => "Success")) . ')';
                } else {
                    echo json_encode(array("Code" => 0, "Message" => "Success"));
                }
            }
        } else {
            if (isset($_GET['callback'])) {
                echo $_GET['callback'] . '(' . json_encode(array("Code" => 500, "Message" => "Authentication failed")) . ')';
            } else {
                echo json_encode(array("Code" => 500, "Message" => "Authentication Failed"));
            }
        }
    }

    private function getActiveChannel($socket, $exten) {

        $originateRequest = "Action: Command\r\n";
        $originateRequest .= "Command: core show channels\r\n";
        $originateRequest .= "Async: yes\r\n\r\n";
        $originate = stream_socket_sendto($socket, $originateRequest);
        stream_set_timeout($socket, 0, 1000);
        $result = stream_get_contents($socket);
        $temp_var = strstr($result, "SIP/$exten");
        $temp = explode(" ", $temp_var);
        return trim($temp[0]);
    }

    private function write_log($logData) {
        $myFile = 'QueueFunctions_log_' . date('Y-m-d') . '.txt';
        $fh = fopen($myFile, 'a+') or die("Please provide permission to log file.");
        $content = $logData . PHP_EOL;
        fwrite($fh, $content);
    }

}

$api = new Calls;
$api->processApi();
?>

