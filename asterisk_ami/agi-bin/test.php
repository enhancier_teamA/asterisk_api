#!/opt/lampp/bin/php -q
<?php
#setting date time zone to India
date_default_timezone_set('Asia/Kolkata');
#setting timout to 30 seconds
set_time_limit(30);

#importing function required for agi init
require('include/phpagi.php');

# Comment during production
error_reporting(E_ALL);

#initializing AGI 
$agi = new AGI();
$agi->answer();
# Calling get_config API using Curl
$url="http://localhost/telephony_functions.php?rquest=get_config";
$url = str_replace(' ', '', $url);
$ch=curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$resp = curl_exec($ch);
curl_close($ch);
$config_arr=json_decode($resp,true); //stored result into config array

	#created individual variables of received data
		$start_time 		=$config_arr["start_time"];
		$end_time 			=$config_arr["end_time"];
		$client 			=$config_arr["client"];
		$welcomefile 		=$config_arr["welcomefile"];
		$no_of_levels   	=$config_arr["no_of_levels"];
		$off_hours_file		=$config_arr["off_hours_file"];
		$next_day_end_flag	=$config_arr["next_day_end_flag"];
		$excluded_days		=$config_arr["excluded_days"];
	if(isset($client) && $client!=NULL){
		$audio_file_path='client_ivr_files/'.$client.'/';

	# IVR time validation and Off hours message playing
			if($start_time!=NULL && $end_time!=NULL && $off_hours_file!=NULL){
				$current_date=date('Y-m-d H:i:s');
				$start_c_time=date('Y-m-d').' '.$start_time;
				$start_c_time=date('Y-m-d H:i:s',strtotime($start_c_time));
				$end_c_time=date('Y-m-d').' '.$end_time;
				$end_c_time=date('Y-m-d H:i:s',strtotime($end_c_time	));
				if($next_day_end_flag==1){
					$tmp = date('Y-m-d',strtotime("+1 day", strtotime(date('Y-m-d'))));	
					$end_c_time=$tmp.' '.$end_time;
				}
				if($current_date<=$start_c_time || $current_date>=$end_c_time ){
					$agi->stream_file($audio_file_path.$off_hours_file);
					sleep(5);
					exit;
				}
				
			}
	# Welcome message validation and execution
		#if welcome file is set then play welcome file
		if($config_arr["welcomefile"] !=NULL){
			$agi->stream_file($audio_file_path.$welcomefile);	
			sleep(5);	
		}
	# Levels message validation and execution
		if($no_of_levels>0){
		$x=0;
			# While loop continues till all ivr level inputs are received or all attempts fails
			while($x!=$no_of_levels){
				$level=$x+1;
				$level_arr=array();
				$url="http://localhost/telephony_functions.php?rquest=get_ivr_level_data&required_level=$level";
				if($x>0){
					for($i=1;$i<=$x;$i++){
						$url.="&level".$i."=".$user_inp[$i];
					}
				}
				$url = str_replace(' ', '', $url);
				$ch=curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				$resp = curl_exec($ch);
				curl_close($ch);
				$level_arr=json_decode($resp,true);
				$arrcount=count($level_arr);
				$user_inp[$level]=0;
				$false_counter=1;
				$flag=0;
				# While loop continues till attempts are exhausted or correct input is taken
				while($false_counter<3 && $flag==0){
					for($i=0;$i<=$arrcount;$i++){
						$ivrfile=$level_arr[$i];	
						$agi->stream_file($audio_file_path.$ivrfile,'#');	
						sleep(2);
					}
					$cnt=0;
					# Due to error in get_data AGI functionality we have put a do while loop for user input
					if($level<2 && $false_counter<2){
						do{
							$result = $agi->get_data('beep', 3000, 1);
							$keys = $result['result'];
							$cnt++;
						}while($keys<=0||$cnt<3);
					
					}else{
						$result = $agi->get_data('beep', 3000, 1);
						$keys = $result['result'];				
					}
					if($keys>0 && $keys<=$arrcount){
						$user_inp[$level]=$keys;
						$flag=1;
					}else{
						$false_counter++;
					}	
				}
				if($false_counter==3){
					$agi->stream_file("something-terribly-wrong");	
					sleep(3);
					exit;
				}
				$x++;
			}
			$selc_queue=json_encode($user_inp);

		# Finding the Queue for input provided by user
			$url="http://localhost/telephony_functions.php?rquest=get_mapped_queue&selections=".$selc_queue;
			$url = str_replace(' ', '', $url);
			$ch=curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$call_queue = curl_exec($ch);
			curl_close($ch);
			$agi->set_variable('CALL_QUEUE',$call_queue);
			return $call_queue;
		}	
	}else{
		echo "Configurations Missing. please set up client before setting IVR";
		exit;
	}

?>
