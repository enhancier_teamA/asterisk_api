<?php
//===========================================================================
// ‘Copyright © 2017, Enhancier Cx Solutions Pvt Ltd.  All rights reserved.
//  File Description          : Agent queue mapping module,telephony location
//  Version                   : V 1.0
//  Current Path              :  http://localhost/asterisk_api_v1/telephony_functions.php
// --------------------------------------------------------------------------
//  Creation Details
//  Date Created              : 05 Dec 2017 12:23
//  Author                    : Mangesh Kamble
// --------------------------------------------------------------------------
//  Change History #1
//  Date Modified             : 
//  Changed By                : 
//  Change Description        : 
//  Ticket Ref Number         : 
//  Reason for Change         : 
//  Updated Code              : 
// --------------------------------------------------------------------------
class Telephony_functions
{
    public $data       = "";
	const DB_SERVER    = "127.0.0.1:3306";
	const DB_USER      = "asterisk";
    const DB_PASSWORD  = "asterisk";
    const DB           = "asterisk";	
    
    private $db = NULL;
    private function dbConnect()
    {
        $this->db = mysql_connect(self::DB_SERVER,self::DB_USER,self::DB_PASSWORD);
        if($this->db){  
            mysql_select_db(self::DB,$this->db);
        }else{
            $error = array('error_code'=>'1','status' => "Fail", "msg" => "Database Conncection Error");
            echo json_encode($error);
	   }
    }
    public function init_function()
    {
    	if(isset($_GET['rquest'])){
            $func=$_GET['rquest'];
            $this->dbConnect();
            $this->$func();
        }else{
            echo "function not found";
        }			
    }
    private function create_CDR()
    {
        $dcontext=isset($_GET['dcontext'])?$_GET['dcontext']:'';
        $dstchannel=isset($_GET['dstchannel'])?$_GET['dstchannel']:'';
        $dst=isset($_GET['dst'])?$_GET['dst']:'';
        $src=isset($_GET['src'])?$_GET['src']:'';
        $clid=isset($_GET['clid'])?$_GET['clid']:'';
        $channel=isset($_GET['channel'])?$_GET['channel']:'';
        $lastapp=isset($_GET['lastapp'])?$_GET['lastapp']:'';
        $lastdata=isset($_GET['lastdata'])?$_GET['lastdata']:'';
        $userfield=isset($_GET['userfield'])?$_GET['userfield']:'';
        $uniqueid=isset($_GET['uniqueid'])?$_GET['uniqueid']:'';
        $accountcode=isset($_GET['accountcode'])?$_GET['accountcode']:'';
        $amaflags=isset($_GET['amaflags'])?$_GET['amaflags']:'';
        $start=isset($_GET['start'])?$_GET['start']:'';
        $answer=isset($_GET['answer'])?$_GET['answer']:'';
        $end=isset($_GET['end'])?$_GET['end']:'';
        $duration=isset($_GET['duration'])?$_GET['duration']:'';
        $billsec=isset($_GET['billsec'])?$_GET['billsec']:'';
        $disposition=isset($_GET['disposition'])?$_GET['disposition']:'';
        $qry="INSERT INTO `cdr`(`calldate`,`clid`,`src`,`dst`,`dcontext`,`channel`,`dstchannel`,`lastapp`,`lastdata`,
                `duration`,`billsec`,`disposition`,`amaflags`,`accountcode`,`uniqueid`,`userfield`) VALUES(NOW(),
                '$clid',
                '$src',
                '$dst',
                '$dcontext',
                '$channel',
                '$dstchannel',
                '$lastapp',
                '$lastdata',
                '$duration',
                '$billsec',
                '$disposition',
                '$amaflags',
                '$accountcode',
                '$uniqueid',
                '$userfield')";
        $x=mysql_query($qry) or die ("error:".mysql_error());
        echo json_encode(array('code' => 0,'flag'=> 'success' ));
    }
    private function get_config()
    {
        $qry=mysql_query("SELECT ivr_setup_id, client, start_time, end_time, welcomefile,no_of_levels,off_hours_file,next_day_end_flag,excluded_days from config");
        if(mysql_num_rows($qry)>0){
            $row=mysql_fetch_assoc($qry);
            $config_arr=array(
            'client'=>$row["client"],
            'start_time'=>$row["start_time"],
            'end_time'=>$row["end_time"],
            'welcomefile'=>$row["welcomefile"],
            'no_of_levels'=>$row["no_of_levels"],
            'off_hours_file'=>$row["off_hours_file"],
            'next_day_end_flag'=>$row["next_day_end_flag"],
            'excluded_days'=>$row["excluded_days"]
            );
            //print_r($config_arr);
            echo json_encode($config_arr);

        }
    }
    private function get_ivr_level_data()
    {
        $result=array();
        $required_level =$_GET['required_level'];
        $sel_table='ivr_level'.$required_level;
        $qry="SELECT ivrfile FROM $sel_table"; 
        if($required_level>1){
        $qry.=" WHERE ";
            for($i=1;$i<$required_level;$i++){
                if($i!=1){
                    $qry.=" AND ";
                }
                $sel_feild='level'.$i.'_id';
                $x='level'.$i;
                $inp_level=$_GET[$x];
                $qry.="$sel_feild=$inp_level ";                      
            }
        }
        $res=mysql_query($qry);
        if(mysql_num_rows($res)>0){
            $i=1;
            while($row=mysql_fetch_assoc($res)){
                $result[$i]=$row['ivrfile'];
                $i++;
            }
        }
        echo json_encode($result);
    }
    private function get_mapped_queue(){
        $selections=$_GET['selections'];
        $i=1;
        $queue='';
        $levels=array();
        $levels=json_decode($selections);
        $qry="SELECT q.name FROM ivr_queue_mapping iqm
                 LEFT JOIN queues q ON q.id=iqm.queue_id WHERE ";
        foreach($levels as $x){
            if($i>1){
                $qry.=" AND ";
            }
            $qry.=" iqm.level".$i."_id = $x ";
            $i++;
        }
        $res=mysql_query($qry);
        if(mysql_num_rows($res)>0){
            $row=mysql_fetch_assoc($res);
            $queue=$row["name"];
        }
        echo $queue;
    }
}
$api = new Telephony_functions;
$api->init_Function();
?>
